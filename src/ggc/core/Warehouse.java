package ggc.core;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.List;
import java.util.TreeMap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import ggc.core.exception.BadEntryException;
import ggc.core.exception.DuplicatedPartnerIdException;
import ggc.core.exception.InvalidCoreDateException;
import ggc.core.exception.NotEnoughStockException;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;
import ggc.core.exception.UnknownTransactionIdException;
import ggc.core.notifications.Notifiable;
import ggc.core.notifications.NotificationType;
import ggc.core.notifications.Notifications;
import ggc.core.partner.Partner;
import ggc.core.products.AggregateProduct;
import ggc.core.products.Batch;
import ggc.core.products.CompareBatchByPrice;
import ggc.core.products.CompareBatches;
import ggc.core.products.Component;
import ggc.core.products.Product;
import ggc.core.products.Recipe;
import ggc.core.products.SimpleProduct;
import ggc.core.transactions.Acquisitions;
import ggc.core.transactions.BreakdownSale;
import ggc.core.transactions.Sale;
import ggc.core.transactions.SaleByCredit;
import ggc.core.transactions.Transactions;
import ggc.core.transactions.Transactions.TransactionType;

/**
 * Class Warehouse implements a warehouse.
 */
public class Warehouse implements Serializable {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 202109192006L;
 
  private double _purchasesBalance = 0;  
  private double _salesPayedBalance = 0; //Amount payed
  private Map<String, Product> _products = new TreeMap<String, Product>();
  private Map<String, Partner> _partners = new TreeMap<String, Partner>();
  private List<Notifiable> _notifiables = new ArrayList<Notifiable>();
  private List<Batch> _batches = new ArrayList<Batch>();
  private List<Transactions> _transactions = new ArrayList<Transactions>();

  /**
   * Constructor for class warehouse 
   */
  public Warehouse() {
    super();
  }
  
  /**
   * overrides serialization to serialize static values
   * @param oos
   * @throws IOException
   */
  private void writeObject(ObjectOutputStream oos) throws IOException {
    oos.defaultWriteObject();
    oos.writeObject(Integer.valueOf(Date.now()));
  }

  /**
   * Overrides deserialization ro read static values
   * @param ois
   * @throws ClassNotFoundException
   * @throws IOException
   */
  private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException  {
    ois.defaultReadObject();
    int days = (Integer)ois.readObject();
    Date.setDays(days); 
  }

  /**
   * 
   * @param id
   * @return a partner with the given id
   * @throws UnknownPartnerIdException
   */
  protected Partner getPartner(String id) throws UnknownPartnerIdException {
    Partner partner = _partners.get(id.toLowerCase());
    if(partner == null)
      throw new UnknownPartnerIdException(id);
    return partner;
  }

  /**
   * 
   * @return collection of partners
   */
  protected Collection<Partner> getPartners() {
    return new ArrayList<Partner>(_partners.values());
  }

  /**
   * 
   * @return all product ids
   */
  private Collection<String> getAllProductIds() {
    return _products.keySet();
  }

  /**
   * 
   * @param id
   * @return a product
   * @throws UnknownProductIdException
   */
  protected Product getProduct(String id) throws UnknownProductIdException {
    Product prod = _products.get(id.toLowerCase());
    if (prod == null)
      throw new UnknownProductIdException(id);
    return prod;
  }

  /**
   * 
   * @param id
   * @return a product whose existence was already confirmed
   */
  private Product getProductThatExists(String id){
    return _products.get(id.toLowerCase());
  }

  /**
   * ! Should only be used inside parser
   * @param id
   * @return true if a product exists
   * 
   */
  protected boolean productExists(String id) {
    return (_products.get(id.toLowerCase()) != null) ? true : false;
  }

  /**
   * 
   * @return a collection with all products
   */
  protected Collection<String> showAllProducts() {
    List<String> l = new ArrayList<>();
    for (String prodId : getAllProductIds()) {
      l.add(_products.get(prodId.toLowerCase()).toString());
    }

    return l;
  }

  /**
   * 
   * @return all batches ordered
   */
  protected Collection<Batch> getAllBatchesOrdered() {
    Collections.sort(_batches, new CompareBatches());
    return new ArrayList<>(_batches);
  }

  /**
   * updates payed balance
   * @param salesPayedBalance
   */
  protected void updateSalesPayedBalance(double salesPayedBalance) {
      _salesPayedBalance += salesPayedBalance;
  }

  /**
   * 
   * @return available balance
   */
  protected double getAvailableBalance() {
    return _salesPayedBalance-_purchasesBalance;
  }

  /**
   * 
   * @return accounting balance
   */
  protected double getAccountingBalance() {
    double expectedPayments = 0;
    for (Transactions t : _transactions) {
      if (t.getType().equals(TransactionType.SALE) || t.getType().equals(TransactionType.BREAKDOWN_SALE)) {
        expectedPayments += ((Sale) t).getAmountPayed();
      }
    }
    return expectedPayments-_purchasesBalance;
  }

  /**
   * @param txtfile filename to be loaded.
   * @throws IOException
   * @throws BadEntryException
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   * @throws DuplicatedPartnerIdException 
   */
  void importFile(String txtfile)
      throws IOException, BadEntryException, UnknownProductIdException, UnknownPartnerIdException, DuplicatedPartnerIdException {
    Parser parse = new Parser(this);
    parse.parseFile(txtfile);
  }

  /**
   * 
   * @param partnerId
   * @return a stringified partner
   * @throws UnknownPartnerIdException
   */
  protected String showPartner(String partnerId) throws UnknownPartnerIdException {
    Partner partner;
    if ((partner = getPartner(partnerId)) == null) {
      throw new UnknownPartnerIdException(partnerId);
    }
    return partner.toString();
  }

  /**
   * 
   * @param partnerId
   * @return notifications for a partner
   * @throws UnknownPartnerIdException
   */
  protected List<String> getNotificationsFrom(String partnerId) throws UnknownPartnerIdException {
    Partner partner = _partners.get(partnerId.toLowerCase());
    if(partner == null)
      throw new UnknownPartnerIdException(partnerId);
    
    return partner.getNotifications();
  }

  /**
   * registers a entity as notifiable
   * @param n
   */
  protected void registerNotifiable(Notifiable n){
    _notifiables.add(n);
    for (Product product : _products.values()) {
      product.toggleNotificationFor(n);
    }
  }

  /**
   * register a new partner
   * @param partnerId
   * @param partnerName
   * @param partnerAddress
   * @throws DuplicatedPartnerIdException
   */
  protected void registerPartner(String partnerId, String partnerName, String partnerAddress) throws DuplicatedPartnerIdException{
    if(_partners.get(partnerId.toLowerCase()) != null)
      throw new DuplicatedPartnerIdException(partnerId);
    Partner partner = new Partner(partnerId, partnerName, partnerAddress);
    _partners.put(partnerId.toLowerCase(), partner);
    registerNotifiable(partner);
  }

  /**
   * sends a notification to all notifiables
   * @param product
   * @param price
   * @param quantity 
   */
  protected void sendNotification(Product product, double price, int quantity) {
    String notificationType = null; 
    
    if (product.getTotalStock() < 0) {
      return;
    }
    if(product.getTotalStock() == 0)
     notificationType  = NotificationType.NEW;
    else if(price < getLowestBatchPriceFor(product))
     notificationType  = NotificationType.BARGAIN; 

    if(notificationType != null) {
      product.notifyObservers(new Notifications(notificationType, product.getId(), price));      
    }
  }

  /**
   * creates a new batch
   * @param product
   * @param partner
   * @param price
   * @param quantity
   * @param parsing
   * @throws IllegalArgumentException
   */
  protected void newBatch(Product product, Partner partner, double price, int quantity, boolean parsing) throws IllegalArgumentException {
    if (quantity < 0)
      throw new IllegalArgumentException("Quantity must be higher than 0");
    if (price < 0)
      throw new IllegalArgumentException("Price must be higher than 0"); 
      if (!parsing) {
        sendNotification(product, price, quantity); 
      }
      _batches.add(new Batch(product, partner, price, quantity));
    }

  /**
   * advances date
   * @param days
   * @throws InvalidCoreDateException
   */
  protected void advanceDate(int days) throws InvalidCoreDateException {
    if (days < 0)
      throw new InvalidCoreDateException(days);
    Date.advanceDays(days);
  }

  /**
   * 
   * @return the current date
   */
  protected int getDate() {
    return Date.now();
  }

  /**
   * registers a new product
   * @param productId
   */
  protected void newSimpleProduct(String productId) {
    try {
      getProduct(productId);
    } catch (Exception e) {
      _products.put(productId.toLowerCase(), new SimpleProduct(productId, _notifiables));
    }
  }

  /**
   * registers a new aggregate product
   * @param productId
   * @param recipe
   * @param aggravation
   */
  protected void newAggregateProduct(String productId, Recipe recipe, double aggravation){
    try {
      getProduct(productId);
    } catch (Exception e) {
      _products.put(productId.toLowerCase(), new AggregateProduct(productId, recipe, aggravation, _notifiables));
    }
  }

  /**
   * 
   * @param partnerId
   * @return
   * @throws UnknownPartnerIdException
   */
  protected Collection<Batch> getBatchesFromPartner(String partnerId) throws UnknownPartnerIdException{
    List<Batch> batchFromPartner = new ArrayList<Batch>();
    for (var batch: _batches){
      if(batch.getPartnerId().equals(getPartner(partnerId).getId()))
        batchFromPartner.add(batch);
    }
    Collections.sort(batchFromPartner, new CompareBatches());
    return batchFromPartner;
  }

  /**
   * 
   * @param productId
   * @return batches
   * @throws UnknownProductIdException
   */
  private Collection<Batch> getBatchesForProduct(String productId) {
    List<Batch> batchForProduct = new ArrayList<Batch>();
    String prodId = getProductThatExists(productId).getId();
    
    for (var batch: _batches){
      if(batch.getProductId().equals(prodId))
        batchForProduct.add(batch);
    }
    Collections.sort(batchForProduct, new CompareBatches()); 
    return batchForProduct;
  }

  protected Collection<Batch> showBatchesForProduct(String productId) throws UnknownProductIdException{
    getProduct(productId);
    return getBatchesForProduct(productId);
  }

  /**
   * updates the purchases
   * @param byAmount
   */
  private void updatePurchases(double byAmount) {
    _purchasesBalance += byAmount;
  }

  /**
   * toggles notifications on and off for a partner
   * @param partnerId
   * @param productId
   * @throws UnknownPartnerIdException
   * @throws UnknownProductIdException
   */
  protected void toggleNotification(String partnerId, String productId) throws UnknownPartnerIdException, UnknownProductIdException{
    Partner partner = getPartner(partnerId);
    Product product = getProduct(productId); //Checks if product exists if not throws error
    product.toggleNotificationFor(partner);
  }

  /**
   * registers a new acquisition
   * @param quantity
   * @param partnerId
   * @param productId
   * @param pricePerUnit
   * @throws UnknownPartnerIdException
   */
  protected  void registerAcquisition(int quantity, String partnerId, String productId, double pricePerUnit) throws UnknownPartnerIdException {
    Partner partner = getPartner(partnerId);
    Product product = getProductThatExists(productId); //product was just created
    updatePurchases((quantity*pricePerUnit));
    _transactions.add(new Acquisitions(quantity, partner, product, pricePerUnit));
    newBatch(product, partner, pricePerUnit, quantity, false);
  }

  /**
   * 
   * @param partnerId
   * @return a collection of acquisitions made by a partner
   * @throws UnknownPartnerIdException
   */
  protected Collection<Transactions> showAcquisitionsByPartner(String partnerId) throws UnknownPartnerIdException{
    Partner partner = getPartner(partnerId);
    List<Transactions> l = new ArrayList<Transactions>();
    for (Transactions transaction : _transactions) {
      if (transaction.getPartnerId().equals(partner.getId()) && transaction.getType().equals(Transactions.TransactionType.ACQUISITIONS)) {
        l.add(transaction);
      }
    }
    return l;
  }

  /**
   * 
   * @param partnerId
   * @return sales made by a partner
   * @throws UnknownPartnerIdException
   */
  protected Collection<Transactions> showSalesByPartner(String partnerId) throws UnknownPartnerIdException{
    Partner partner = getPartner(partnerId);
    List<Transactions> l = new ArrayList<Transactions>();
    for (Transactions transaction : _transactions) {
      if (transaction.getPartnerId().equals(partner.getId()) && 
          (transaction.getType().equals(Transactions.TransactionType.SALE) || 
          transaction.getType().equals(Transactions.TransactionType.BREAKDOWN_SALE))) {

        l.add(transaction);
      }
    }
    return l;
  }

  /**
   * 
   * @param transactionId
   * @return returns a transaction with the given id
   * @throws UnknownTransactionIdException
   */
  protected Transactions getTransaction(int transactionId) throws UnknownTransactionIdException {
    Transactions t = null;
    try {
      t = _transactions.get(transactionId); 
    } catch (Exception e) {
      throw new UnknownTransactionIdException(transactionId);
    }
    
    return t;
  }

  /**
   * 
   * @param priceLimit
   * @return batches under a price
   */ 
  protected Collection<Batch> showBatchesUnderPrice(double priceLimit) {
    List<Batch> l = new ArrayList<Batch>(); 
    for (Batch batch : _batches) {
      if(batch.getPrice() < priceLimit)
        l.add(batch);
    }

    Collections.sort(l, new CompareBatches());
    return l;
  }

  /**
   * 
   * @param product
   * @return the lowest batch price for a given product
   */
  private double getLowestBatchPriceFor(Product product){
    List<Batch> batches = new ArrayList<>(getBatchesForProduct(product.getId())); 
    if(batches.isEmpty())
      return product.getMaxPrice();

    Collections.sort(batches, new CompareBatchByPrice());
    return batches.get(0).getPrice();

  }
  /**
   * 
   * @param product
   * @param partner
   * @param quantity
   * @return the breakdown price
   */
  private double calculatePriceBreakdown(Component component, Partner partner, int breakdownQuantity){
    double cost = getLowestBatchPriceFor(component.getProduct());
    newBatch(component.getProduct(), partner, cost, component.getQuantity()*breakdownQuantity,false);
    return cost*breakdownQuantity*component.getQuantity();
  }

  /**
   * this function assumes to have the guarantee that there is enough stock to do breakdown
   * @param neededStock
   * @param partner
   * @param product
   * @throws NotEnoughStockException
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   */
  protected void registerBreakdownSale(int neededStock, String partnerId, String productId) throws NotEnoughStockException, UnknownProductIdException, UnknownPartnerIdException{
    double costBeforeBreakdown = 0;
    double componentCost = 0;
    String str = "";
    double netCost = 0;
    Partner partner = getPartner(partnerId);
    Product tmpProduct = getProduct(productId);
    
    if(neededStock > tmpProduct.getTotalStock())
      throw new NotEnoughStockException(productId, neededStock, tmpProduct.getTotalStock());
    if(tmpProduct.isSimpleProduct())   
      return;  
    AggregateProduct product = (AggregateProduct) tmpProduct;
    
    //Cost before breakdown and batch disassembly
    costBeforeBreakdown = sellProduct(new ArrayList<>(getBatchesForProduct(product.getId())), product, neededStock); 
    

    //New costs for batches from aggregate product result of breakdown
    for (Component c : product.getComponents()) { 
      double localComponentCost = calculatePriceBreakdown(c, partner, neededStock);
      componentCost += localComponentCost;
      str += String.format("%s:%d:%d", c.getId(),c.getQuantity()*neededStock, (int) Math.round(localComponentCost));
      //if not in last component add '#'
      if (!product.getComponents().get(product.getComponents().size()-1).getId().equals(c.getId()))
        str+='#';      
    }
    netCost = costBeforeBreakdown-componentCost;
    _transactions.add(new BreakdownSale(neededStock, partner, product, netCost, str));
    if(netCost < 0)
      netCost = 0;
    partner.addPoints(10*netCost);
    updateSalesPayedBalance(netCost); 

    return;
  }

  /**
   * 
   * @param batches
   * @param product
   * @param neededStock
   * @return the price for the needed stock based on batch prices and max historical price for a given product
   */
  private double calculatePrice(List<Batch> batches, Product product, int neededStock){
    double totalPrice = 0;
    
    Collections.sort(batches, new CompareBatchByPrice());
    for (Batch batch : batches) {
      if(neededStock == 0){
        return totalPrice;
      }
      totalPrice += batch.calculatePrice(neededStock);
      neededStock = batch.calculateNeededStock(neededStock);
      if(batch.getQuantity() == 0) {
        _batches.remove(batch); 
      }
    }
    return totalPrice;
  }


  /**
   * 
   * @param product
   * @param neededStock stock needed
   * @return the price required to fabricate the amount of needed stock
   */
  private double calculateFabricateProductPrice(AggregateProduct product, int neededStock) {
    double cost = 0;
    for (Component component : product.getComponents()) {
      cost += sellProduct(new ArrayList<Batch>(getBatchesForProduct(component.getId())), 
                           getProductThatExists(component.getId()), 
                           neededStock*component.getQuantity());  
    }
    return (1 + product.getAggravation())*cost;
  }

  /**
   * updates the price of a aggregate product if it aggregates
   * @param batches
   * @param product
   * @param quantity
   * @return the price of the sale
   */
  private double sellProduct(List<Batch> batches, Product product, int quantity){
    int neededStock = 0;
    double totalPrice = 0;
    //check if it is an Aggregate Product and if there is enough stock or there needs to be an aggregation
    if (product.getAvailableStock() != product.getTotalStock() && quantity > product.getTotalStock()) {
      neededStock = quantity-product.getTotalStock();
      totalPrice += calculateFabricateProductPrice((AggregateProduct) product, neededStock);  
      product.updatePrice(totalPrice/neededStock); //undo sum of all costs
    }

    return totalPrice + calculatePrice(batches, product, quantity-neededStock);
  }

  /**
   * Creates new sale transaction
   * updates sales for partner and warehouse
   * @param partner
   * @param product
   * @param requestedAmount
   * @param requestedPaymentDeadline
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   * @throws InvalidCoreDateException
   * @throws NotEnoughStockException
   */
  protected void registerSaleByCredit(String partnerId, String productId, int requestedAmount, int requestedPaymentDeadline) throws UnknownProductIdException, UnknownPartnerIdException, InvalidCoreDateException, NotEnoughStockException{
    List<Batch> batches = null;
    double cost = 0; 
    Partner partner = getPartner(partnerId);
    Product product = null;
    
    if(requestedPaymentDeadline < 0)
      throw new InvalidCoreDateException(requestedPaymentDeadline);
    product = getProduct(productId);
    batches = new ArrayList<Batch>(getBatchesForProduct(product.getId())); 
    
    product.hasEnoughStockFor(requestedAmount);
    
    cost = sellProduct(batches, product, requestedAmount);
    _transactions.add(new SaleByCredit(requestedAmount, partner, product, requestedPaymentDeadline, cost));
  }

  /**
   * 
   * @param transaction
   * @return true if a sale has been paid
   */
  private boolean isSalePaid(Transactions transaction) {
    if (transaction.getType().equals(Transactions.TransactionType.SALE))
      return ((SaleByCredit)transaction).isPaid();
    return false;
  }

  /**
   * Receives payment for a sale transaction
   * if it is not sale transaction does nothing
   * @param transaction sale transaction
   * @throws UnknownTransactionIdException
   */
  protected void receivePayment(int transactionId) throws UnknownTransactionIdException { 
    Transactions transaction = getTransaction(transactionId);
    if (isSalePaid(transaction)) 
      return;
    
    if (transaction.getType().equals(Transactions.TransactionType.SALE)){
      updateSalesPayedBalance(((SaleByCredit) transaction).priceToPayWithDueDate());
    }
      return;
  }

  /**
   * 
   * @param partnerId
   * @return the payments made by partner with partner id x
   * @throws UnknownPartnerIdException
   */
  protected Collection<Transactions> showPaymentsByPartner(String partnerId) throws UnknownPartnerIdException{
    Partner partner = getPartner(partnerId);
    List<Transactions> l = new ArrayList<>();

    for (Transactions transaction : _transactions) {
      if(transaction.getPartnerId().equals(partner.getId())){
        if (!transaction.getType().equals(TransactionType.ACQUISITIONS) && ((Sale) transaction).isPaid()) {
          l.add(transaction);
        }
      }
    }

    return l;
  }
}