package ggc.core.exception;

/**
 * Exception for unknown partner ids
 */
public class UnknownPartnerIdException extends Exception {
    /**
     * 
     * @param partnerId
     */
    public UnknownPartnerIdException(String partnerId) {
        super("Unknown product id "+partnerId);
    }

}
