package ggc.core.exception;

/**
 * UnknownProductID
 */
public class UnknownProductIdException extends Exception{
    /**
     * 
     * @param productId
     */
    public UnknownProductIdException(String productId) {
        super("Unknown product id "+productId);
    }
    
}