package ggc.core.exception;

/**
 * Exception for when there is not enough stock
 */
public class NotEnoughStockException extends Exception{
    private String _productId = "";
    private int _neededStock = 0;
    private int _availableStock = 0;
    /**
     * 
     * @param days
     * @param neededStock
     * @param availableStock
     */
    public NotEnoughStockException(String productId, int neededStock, int availableStock) {
        super();
        _productId = productId;
        _neededStock = neededStock;
        _availableStock = availableStock;
    }

    /**
     * 
     * @return available stock of the warehouse
     */
    public int getAvailableStock() {
        return _availableStock;
    }
    /**
     * 
     * @return the amount of stock requested
     */
    public int getNeededStock() {
        return _neededStock;
    }

    /**
     * 
     * @return id of the product
     */
    public String getProductId() {
        return _productId;
    }

    
}