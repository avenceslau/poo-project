package ggc.core.exception;

/**
 * Exception for unknown transaction ids
 */
public class UnknownTransactionIdException extends Exception {
    /**
     * 
     * @param transactionId
     */
    public UnknownTransactionIdException(int transactionId) {
        super("Unknown transaction id "+transactionId);
    }
}