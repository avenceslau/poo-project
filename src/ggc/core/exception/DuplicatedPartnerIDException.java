package ggc.core.exception;

/**
 * Exception for duplicated partner ids
 */
public class DuplicatedPartnerIdException extends Exception {
    /**
     * 
     * @param partnerId
     */
    public DuplicatedPartnerIdException(String partnerId) {
        super("Duplicated partner id: " + partnerId);
    }

}