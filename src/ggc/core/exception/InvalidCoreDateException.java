package ggc.core.exception;

/**
 * InvalidDate
 */
public class InvalidCoreDateException extends Exception{
    /**
     * 
     * @param days
     */
    public InvalidCoreDateException(int days) {
        super("Advance by must be higher than 0. "+days+" is not higher than 0");
    }
    
}