package ggc.core.notifications;

/** Notification type entries. */
public interface NotificationType {

    String BARGAIN = "BARGAIN";
    String NEW = "NEW";

}