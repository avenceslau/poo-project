package ggc.core.notifications;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * NotificationManager
 */
public class NotificationManager implements Serializable {
    /** Serial number for serialization. */
    private static final long serialVersionUID = 25207840384032370l;
    private Set<Notifiable> _notifiables = new HashSet<>();

    /**
     * 
     * @param n
     * @return true if the notifiable was added to the list of notifiables
     */
    public boolean addNotifiable(Notifiable n){
        return _notifiables.add(n);
    }

    /**
     * 
     * @param n
     * @return true if the notifiable was removed of the list of notifiables
     */
    public boolean removeNotifiable(Notifiable n){
        return _notifiables.remove(n);
    }

    /**
     * 
     * @param n
     * @return true if notifiable receives notifications
     */
    public boolean receivesNotifications(Notifiable n){
        return _notifiables.contains(n);
    }

    /**
     * sends the notification based on the send mode
     * @param notification
     */
    public void notifyAllNotifiables(Notifications notification){
        for (Notifiable notifiable : _notifiables) {
            notifiable.getSendMode().sendNotification(notifiable, notification);
        }
    }
}