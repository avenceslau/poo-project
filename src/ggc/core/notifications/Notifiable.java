package ggc.core.notifications;

/**
 * Notifiable
 */
public interface Notifiable {
    /**
     * adds a notification
     * @param notification
     */
    public void addNotifications(Notifications notification);
    /**
     * changes the send mode
     * @param newMode
     */
    void setSendMode(SendNotificationMode newMode);

    /**
     * 
     * @return the send mode
     */
    SendNotificationMode getSendMode();
}