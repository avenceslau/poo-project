package ggc.core.notifications;

import java.io.Serializable;

/**
 * Notifications
 */
public class Notifications implements Serializable {
    /** Serial number for serialization. */
    private static final long serialVersionUID = 2120784038403201210l;
    
    private String _notificationType = "";
    private String _productId = "";
    private double _productPrice = 0;

    /**
     * 
     * @param notificationType
     * @param productId
     * @param productPrice
     */
    public Notifications(String notificationType, String productId, double productPrice) {
        _notificationType = notificationType;
        _productId = productId;
        _productPrice = productPrice;
    }

    /**
     * 
     * @return the notification type
     */
    public String getNotificationType() {
        return _notificationType;
    }

    /**
     * 
     * @return the id of the product associated to the notification
     */
    public String getProductId() {
        return _productId;
    }

    /**
     * 
     * @return the price of the product when the notification was sent
     */
    public double getProductPrice() {
        return _productPrice;
    }

    @Override
    public String toString() {
        return getNotificationType()+"|"+getProductId()+"|"+(int) Math.round(getProductPrice());
    }
}