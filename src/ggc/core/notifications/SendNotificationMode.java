package ggc.core.notifications;

/**
 * SendNotificationMode
 */
public interface SendNotificationMode {
    /**
     * Sends the notification based on the desired notification destination
     * @param n
     * @param notification */
    void sendNotification(Notifiable n, Notifications notification);
}