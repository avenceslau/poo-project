package ggc.core.notifications;

import java.io.Serializable;

/**
 * NotificationToScreen
 */
public class NotificationToScreen implements SendNotificationMode, Serializable {
    private static final long serialVersionUID = 21201210992391238l;
    /**
     * Sends the notification to a list of notifications
     * @param n
     * @param notification */
    @Override
    public void sendNotification(Notifiable n, Notifications notification) {
        n.addNotifications(notification);
    }
}