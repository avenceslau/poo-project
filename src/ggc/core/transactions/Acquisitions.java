package ggc.core.transactions;

import ggc.core.Date;
import ggc.core.partner.Partner;
import ggc.core.products.Product;

/**
 * Acquisitions
 */
public class Acquisitions extends Transactions {
    private double _cost = 0;

    /**
     * constructor for acquisitions
     * @param quantity
     * @param partner
     * @param product
     * @param pricePerUnit
     */
    public Acquisitions(int quantity, Partner partner, Product product, double pricePerUnit) {
        super(quantity, partner, product, TransactionType.ACQUISITIONS);
        _cost =  pricePerUnit*quantity;
        partner.updatePurchases(_cost);
        setPaymentDate(Date.now());
    }

    /**
     * 
     * @return the middle part of the string that changes for each transaction
     */
    @Override
    protected String middleString() {
        return String.format("%d", (int) Math.round(_cost));
    }
}