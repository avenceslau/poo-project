package ggc.core.transactions;

import ggc.core.partner.Partner;
import ggc.core.products.Product;

public abstract class Sale extends Transactions {
	private double _baseValue = 0;
	private double _amountPayed = 0;
	private boolean _alreadyPayed = false;
	
	/**
	* Constructor for Sale
	*/
	protected Sale(int quantity, Partner partner, Product product, double cost, TransactionType type) {
		super(quantity, partner, product, type);
		_baseValue = cost;	
	}

	/**
	 * 
	 * @return sale base value without discounts
	 */	
	protected double getBaseValue() {
        return _baseValue;
    }

	/**
     * 
     * @return the middle part of the string that changes for each transaction
     */
	protected abstract String middleString();
	/**
	 * 
	 * @return the end part of the string
	 */
	protected abstract String endString();

	@Override
	public String toString() {
		return super.toString()+endString();
	}

	/**
	 * 
	 * @return true if the transaction was payed
	 */
	public boolean isPaid() {
		return _alreadyPayed;
	}

	/**
	 * 
	 * @return the amount payed by a partner
	 */
	public double getAmountPayed() {
		return _amountPayed;
	}

	/**
	 * sets sale has already payed
	 */
	protected void setAlreadyPayed(double cost) {
		_alreadyPayed = true;
		if (cost < 0) 
			cost = 0;
		_amountPayed = cost;
	}
}