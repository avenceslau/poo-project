package ggc.core.transactions;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ggc.core.Date;
import ggc.core.Period;
import ggc.core.Periods;
import ggc.core.partner.Partner;
import ggc.core.products.Product;
public class SaleByCredit extends Sale {

	private int _paymentDate = 0;
	
	/**
     * Overrides write serializable to be able to serialize static values from date and global id
     * @param oos
     * @throws IOException
     */
	private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(Integer.valueOf(_paymentDate));
        
      }
    
	/**
     * Overrides read serializable to be able to serialize static values from date and global id 
     * @param ois
     * @throws ClassNotFoundException
     * @throws IOException
     */	
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException  {
    	ois.defaultReadObject();
        _paymentDate = (Integer) ois.readObject();
        
    }

	/**
	* Constructor for SaleByCredit
	*/
	public SaleByCredit(int quantity, Partner partner, Product product, int dueDate, double cost) {
		super(quantity, partner, product, cost, TransactionType.SALE);
		setPaymentDate(dueDate);
		partner.updateTotalSales(cost);
	}

	
	/**
	 * 
	 * @return the due date
	 */
	private int getDueDate(){
		return getPaymentDate();
	}

	/**
	 * 
	 * @return the amount payed by a partner
	 */
	@Override
	public double getAmountPayed() {
		double cost = getBaseValue()*getPartner().getCostModifier(getDueDate(), getN());
		if (isPaid()) {
			cost = super.getAmountPayed();
		}
		return cost;
	}

	/**
     * 
     * @return the middle part of the string that changes for each transaction
     */
    @Override
    public String middleString() {
		String str = String.format("%d|%d", (int) Math.round(getBaseValue()), (int) Math.round(getAmountPayed()));
        return str;
    }

	/**
     * 
     * @return the middle part of the string that changes for each transaction
     */
	@Override
	public String endString() {
		String str = "";
		if(isPaid())
			str = "|"+_paymentDate;
        return str;
	}

	/**
	 * 
	 * @return 5 if is simple product or 3 if is aggregate
	 */
	private int getN(){
		if (getProduct().isSimpleProduct()) {
			return 5;
		}
		return 3;
	}

	/**
	 * 
	 * @param cost
	 * @return the number of points based on the date
	 */
	private double receivePoints(double cost) {
        Period days = Periods.dateDifferential(getN(), getDueDate());
        if (days == Period.P1 || days == Period.P2)
            return cost * 10;
		if (days == Period.P3 || days == Period.P4)
            getPartner().losePoints(getDueDate());
        return 0;
    }
	
	/**
	 * updates partner points
	 * updates payed sales
	 * @return the cost after the discounts/fines are applied
	 */
	public double priceToPayWithDueDate() {
		double cost = getBaseValue()*getPartner().getCostModifier(getDueDate(), getN());
		getPartner().addPoints(receivePoints(cost));
		getPartner().updatePayedSales(cost);
		setAlreadyPayed(cost);
		_paymentDate = Date.now();
		return cost;
	}
}