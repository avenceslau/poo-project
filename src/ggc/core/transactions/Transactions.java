package ggc.core.transactions;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import ggc.core.partner.Partner;
import ggc.core.products.Product;

/**
 * Transactions
 */
public abstract class Transactions implements Serializable {
    private static int _globalId = 0;
    private int _id = _globalId;
    private int _paymentDate = 0;
    private int _quantity = 0;
    private Partner _partner = null;
    private Product _product = null;
    private TransactionType _type = null;
    
    public enum TransactionType {
        ACQUISITIONS("COMPRA"),
        SALE("VENDA"),
        BREAKDOWN_SALE("DESAGREGAÇÃO");
    
        private final String _label;
    
        private TransactionType(String label) {
            _label = label;
        }

        protected String getLabel(){
            return _label;
        }
    }

    /**
     * Is used inside each kind of transaction to set the date of the transaction
     * @param date
     */
    protected void setPaymentDate(int date){
        _paymentDate = date;
    }

    /**
     * Overrides write serializable to be able to serialize static values from date and global id
     * @param oos
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(Integer.valueOf(_paymentDate));
        oos.writeObject(Integer.valueOf(_id));
        oos.writeObject(Integer.valueOf(_globalId));
      }
    
    /**
     * Overrides read serializable to be able to serialize static values from date and global id 
     * @param ois
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException  {
        ois.defaultReadObject();
        _paymentDate = (Integer) ois.readObject();
        _id = (Integer) ois.readObject();
        if(_globalId == 0)
            _globalId = (Integer) ois.readObject();
    }
    
    /**
     * 
     * @return the middle part of the string that changes for each transaction
     */
    protected abstract String middleString();
    

    /**
     * 
     * @param quantity
     * @param partner
     * @param product
     */
    public Transactions(int quantity, Partner partner, Product product, TransactionType type)  {
        super();
        _product = product;
        _partner = partner;
        _quantity = quantity;
        _globalId++;
        _type = type;
    }

    /**
     * 
     * @return the type of the transaction
     */
    public TransactionType getType() {
        return _type;
    }

    /**
     * 
     * @return the day of payment
     */
    protected int getPaymentDate(){
        return _paymentDate;
    }   

    /**
     * 
     * @return transaction id
     */
    protected int getId() {
        return _id;
    }

    /**
     * 
     * @return quantity of product involved in transaction
     */
    protected int getQuantity() {
        return _quantity;
    }

    /**
     * 
     * @return partner involved in the transaction
     */
    protected Partner getPartner() {
        return _partner;
    }

    /**
     * 
     * @return the id of the partner involved in the transaction
     */
    public String getPartnerId(){
        return _partner.getId();
    }

    /**
     * 
     * @return the id of the product involved in the transaction
     */
    protected String getProductId(){
        return _product.getId();
    }

    /**
     * 
     * @return the product involved in the transaction
     */
    protected Product getProduct() {
        return _product;
    }
        
    @Override
    public String toString() {
        return String.format("%s|%d|%s|%s|%d|%s|%d", getType().getLabel(), getId(), getPartnerId(), getProductId(), getQuantity(), middleString(), getPaymentDate());
    }
}