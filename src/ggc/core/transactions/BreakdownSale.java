package ggc.core.transactions;

import ggc.core.Date;
import ggc.core.partner.Partner;
import ggc.core.products.Product;

public class BreakdownSale extends Sale {

	private String _componentsString = "";

	/**
	* Constructor for BreakdownSale
	*/
	public BreakdownSale(int quantity, Partner partner, Product product, double cost, String componentsString) {
		super(quantity, partner, product, cost, TransactionType.BREAKDOWN_SALE);
		_componentsString = componentsString;
		setPaymentDate(Date.now());
		setAlreadyPayed(cost);	
	}

	/**
	 * 
	 * @return the end part of the string
	 */
    @Override
    protected String middleString() {
		double payedByPartner = 0;
		if (getBaseValue() > 0) {
			payedByPartner = getBaseValue();
		}
    	return String.format("%d|%d", (int) Math.round(getBaseValue()), (int) Math.round(payedByPartner));
    }
	
	/**
	 * 
	 * @return the end part of the string
	 */
	@Override
	protected String endString() {
		return "|"+_componentsString;
	}
}