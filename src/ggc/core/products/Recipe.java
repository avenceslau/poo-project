package ggc.core.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ggc.core.exception.NotEnoughStockException;

/**
 * Recipe
 */
public class Recipe implements Serializable{
    private List<Component> _components = new ArrayList<Component>();
    private static final long serialVersionUID = 2120198796263869210l;
    /**
     * Constructor for Recipe
     * @param products 
     * @param quantities
     */
    public Recipe(List<Product> products, List<Integer> quantities) {
        super();
        for(int i = 0; i < products.size() ; i++) {
            _components.add(new Component(products.get(i), quantities.get(i)));
        }   
    }

    /**
     * 
     * @return List of all components
     */
    protected List<Component> getComponents(){
        return new ArrayList<>(_components);
    }

    @Override
    public String toString(){
        String recipeString = "";
        for (int i = 0 ; i < _components.size() ; i++) {
            Component tmp = _components.get(i);
            recipeString += tmp.toString();
            if(i == _components.size()-1)
                break;
            recipeString += "#";
        }
        return recipeString;
    }

    /**
     * 
     * @param requestedAmount
     * @return true if there is enough stock for the requested amounted
     */
    protected boolean hasEnoughStockFor(int requestedAmount) throws NotEnoughStockException {
        for (Component component : _components) {
            if (component.getMaxPossibleStock() < requestedAmount) {
                throw new NotEnoughStockException(component.getId(), requestedAmount*component.getQuantity(), component.getProduct().getTotalStock());
            }
        }
        return true;
    }

    /**
     * 
     * @return max possible stock that can be produced for te recipe
     */
    protected int getAvailableStock(){
        if (_components.isEmpty())
            return 0;

        int maxPossibleStock = _components.get(0).getMaxPossibleStock();
        
        for (Component component : _components) {  
            maxPossibleStock = Integer.min(maxPossibleStock, component.getMaxPossibleStock());
        }

        return maxPossibleStock;
    }
    
}
