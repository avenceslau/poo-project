package ggc.core.products;

import java.util.Comparator;

/**
 * CompareBatchByPrice
 */
public class CompareBatchByPrice implements Comparator<Batch> {
    @Override
    public int compare(Batch o1, Batch o2) {
        return Double.compare(o1.getPrice(), o2.getPrice());
    }
    
}