package ggc.core.products;

import java.io.Serializable;

import ggc.core.partner.Partner;

/**
 * Batch
 */
public class Batch implements Serializable {
    private static final long serialVersionUID = 21201213123l;
    private Product _product = null;
    private Partner _partner = null;
    private double _price = 0;
    private int _quantity = 0;

    /**
     * 
     * @param product
     * @param partner
     * @param price must be higher than 0
     * @param quantity must be higher than 0
     */
    public Batch(Product product, Partner partner, double price, int quantity) {
        _product = product;
        _partner = partner;
        if(price < 0)
            throw new IllegalArgumentException("Only positive prices allowed"); 
        _price = price;
        if(quantity < 0)
            throw new IllegalArgumentException("Only positive quantities allowed");
        _quantity = quantity;
        _product.updateTotalStockBy(quantity);;
        _product.updatePrice(price);
        
    }

    @Override
    public String toString() {
        return getProductId()+"|"+getPartnerId()+"|"+(int) Math.round(getPrice())+"|"+getQuantity();
    }

    /**
     * 
     * @return the id of the partner associated to the batch
     */
    public String getPartnerId() {
        return _partner.getId();
    }

    /**
     * 
     * @return price of the batch
     */
    public double getPrice() {
        return _price;
    }

    /**
     * 
     * @return the id of the product
     */
    public String getProductId() {
        return _product.getId();
    }

    /**
     * 
     * @return the stock of the product
     */
    public int getQuantity() {
        return _quantity;
    }

    /**
     * sets the quantity
     * @param quantity
     */
    private void setQuantity(int quantity) {
        _quantity = quantity;
    }

    
    /**
     * 
     * @param neededStock
     * @return of the amount of stock still needed
     */
    public int calculateNeededStock(int neededStock){
        if (getQuantity() < neededStock) {
            _product.updateTotalStockBy(-getQuantity());
            neededStock = neededStock - getQuantity();
            setQuantity(0);
        } else {
            setQuantity(getQuantity()-neededStock);
            _product.updateTotalStockBy(-neededStock);
            neededStock = 0;
        }
        
        return neededStock;
    }

    /**
     * 
     * @param neededStock
     * @return of the amount of stock still needed
     */
    public double calculatePrice(int neededStock){
        double price = 0;
        if (getQuantity() < neededStock) {
            price = getQuantity()*getPrice();
        } else {
            price = neededStock*getPrice();
        }
        return price;
    }
    
}