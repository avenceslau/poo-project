package ggc.core.products;
import java.util.Comparator;

/**
 * CompareBatchesByProduct extends Comparator
 */
public class CompareBatches implements Comparator<Batch> {
    @Override
    public int compare(Batch a, Batch b) {
        //If id is equal
        if(a.getProductId().toLowerCase().equals(b.getProductId().toLowerCase())) {
            //If partner is equal
            if (a.getPartnerId().toLowerCase().equals(b.getPartnerId().toLowerCase())) {
                //If price is equal
                if(a.getPrice() == b.getPrice()){
                    return Integer.compare(a.getQuantity(),b.getQuantity());
                }
                return Double.compare(a.getPrice(), b.getPrice());
            }
            return a.getPartnerId().toLowerCase().compareTo(b.getPartnerId().toLowerCase());    
        }
        return a.getProductId().toLowerCase().compareTo(b.getProductId().toLowerCase());
    } 
}