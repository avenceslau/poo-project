package ggc.core.products;

import java.util.List;

import ggc.core.notifications.Notifiable;

/**
 * SimpleProduct
 */
public class SimpleProduct extends Product {
    /**
     * 
     * @param id productId
     */
    public SimpleProduct(String id, List<Notifiable> notifiables) {
        super(id, notifiables);
    } 
}