package ggc.core.products;

import java.util.List;

import ggc.core.exception.NotEnoughStockException;
import ggc.core.notifications.Notifiable;

/**
 * AggregateProduct
 */
public class AggregateProduct extends Product {
    private Recipe _recipe;
    private double _aggravation;

    /**
     * 
     * @param id
     * @param recipe
     * @param aggravation must be higher than 0
     * @throws IllegalArgumentException 
     */
    public AggregateProduct(String id, Recipe recipe, double aggravation, List<Notifiable> notifiables) throws IllegalArgumentException{
        super(id, notifiables);
        _recipe = recipe;
        if (aggravation < 0) {
            throw new IllegalArgumentException("Aggravation must be higher than 0");
        }
        _aggravation = aggravation;
    }

    /**
     * 
     * @return a list of components
     */
    public List<Component> getComponents(){
        return _recipe.getComponents();
    }

    @Override
    public String toString(){
        return super.toString()+"|"+_recipe.toString();
    }

    /**
     * takes into account the amount of product that can be aggregated
     * @param requestedAmount
     * @return true if there is enough stock for the requested amount
     */
    @Override
    public boolean hasEnoughStockFor(int requestedAmount) throws NotEnoughStockException{
        int neededStock = requestedAmount-getTotalStock();
        if (neededStock <= 0) {
            return true;
        }
        return _recipe.hasEnoughStockFor(neededStock);
    }

    @Override
    /**
     * calculates the amount of stock already produced + the producible stock
     */
    public int getAvailableStock() {
        return super.getAvailableStock()+_recipe.getAvailableStock();
    }

    /**
     * 
     * @return aggravation
     */
    public double getAggravation() {
        return _aggravation;
    }

    /**
     * 
     * @return true if is simple product, returns false
     */
    @Override
    public boolean isSimpleProduct() {
        return false;
    }
}