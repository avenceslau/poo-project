package ggc.core.products;

import java.io.Serializable;

/**
 * Components
 */
public class Component implements Serializable{
    private static final long serialVersionUID = 21201210l;
    private Product _prod;
    private int _quantity;

    /**
     * 
     * @param prod
     * @param quantity
     */
    protected Component(Product prod, int quantity) {
        super();
        _prod = prod;
        _quantity = quantity;
    }

    
    /**
     * 
     * @return the product id
     */
    public String getId() {
        return _prod.getId();
    }

    /**
     * 
     * @return the product
     */
    public Product getProduct() {
        return _prod;
    }

    /**
     * 
     * @return the quantity needed for the component
     */
    public int getQuantity(){
        return _quantity;
    }

    @Override
    public String toString(){
        return getId()+":"+_quantity;
    }

    /**
     * 
     * @return the maximum of stock stack can bee aggregated for this component
     */
    protected int getMaxPossibleStock() {
        return _prod.getAvailableStock()/_quantity;
    }
}