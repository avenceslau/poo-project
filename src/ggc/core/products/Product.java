package ggc.core.products;

import java.io.Serializable;
import java.util.List;

import ggc.core.exception.NotEnoughStockException;
import ggc.core.notifications.Notifiable;
import ggc.core.notifications.NotificationManager;
import ggc.core.notifications.Notifications;

/**
 * Product
 * ! When interacting with Map convert _id to lowercase
 */
public abstract class Product implements Serializable {
    private static final long serialVersionUID = 212012199909l; 
    private double _maxPrice = 0; 
    private String _id = "";
    private int _totalStock = -1;
    private NotificationManager _notificationManager = new NotificationManager();

    /**
     * Constructor
     */
    Product(String id, List<Notifiable> notifiables) {
        super();
        _id = id;
        for (Notifiable notifiable : notifiables) {
            toggleNotificationFor(notifiable);
        }
    }

    /**
     * Stringify Product
     */
    public String toString(){
        return _id+"|"+(int) Math.round(_maxPrice)+"|"+ (int) Math.round(_totalStock);
    }

    /**
     * 
     * @return historical max price
     */
    public double getMaxPrice(){
        return _maxPrice;
    }

    /**
     * 
     * @return thr product id
     */
    public String getId(){
        return _id;
    }    

    /**
     * 
     * @return the total stock
     */
    public int getTotalStock() {
        return _totalStock;
    }

    /**
     * increases total stock
     * @param stock
     */
    protected void updateTotalStockBy(int stock){
        if (_totalStock == -1) {
            _totalStock++;
        }
        _totalStock += stock;
    }

    
    /**
     * update max price
     * @param newPrice
     */
    public void updatePrice(double newPrice){
        if(newPrice > _maxPrice)
            _maxPrice = newPrice;
    }

    /**
     * 
     * @param requestedAmount
     * @return true if there is enough stock for the requested amount
     */
    public boolean hasEnoughStockFor(int requestedAmount) throws NotEnoughStockException{
        if(requestedAmount > _totalStock)
            throw new NotEnoughStockException(getId(), requestedAmount, getAvailableStock());
        return true;
    }

    /**
     * can be used to check producible stock in aggregate products
     * @return available stock
     */
    public int getAvailableStock() {
        return _totalStock;
    }

    /**
     * notifies all observers
     * @param notification
     */
    public void notifyObservers(Notifications notification){
        _notificationManager.notifyAllNotifiables(notification);    
    }

    /**
     * toggles notifications for a notifiable entity
     * @param n
     */
    public void toggleNotificationFor(Notifiable n){
        if(_notificationManager.receivesNotifications(n)) {
            _notificationManager.removeNotifiable(n);
            return;
        }
        _notificationManager.addNotifiable(n);
    
    }

    /**
     * 
     * @return true if is simple product
     */
    public boolean isSimpleProduct(){
        return true;
    }
}

