package ggc.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.BufferedReader;

import ggc.core.exception.BadEntryException;
import ggc.core.exception.DuplicatedPartnerIdException;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;
import ggc.core.partner.Partner;
import ggc.core.products.Product;
import ggc.core.products.Recipe;


public class Parser {

  private Warehouse _warehouse;

  /**
   * 
   * @param w
   */
  public Parser(Warehouse w) {
    _warehouse = w;
  }

  /**
   * 
   * @param filename
   * @throws IOException
   * @throws BadEntryException
   * @throws UnknownProductIdException
   * @throws DuplicatedPartnerIdException
   * @throws UnknownPartnerIdException
   */
  void parseFile(String filename) throws IOException, BadEntryException, UnknownProductIdException, DuplicatedPartnerIdException, UnknownPartnerIdException {
    try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
      String line;

      while ((line = reader.readLine()) != null)
        parseLine(line);
    }
  }

  /**
   * 
   * @param line
   * @throws BadEntryException
   * @throws UnknownProductIdException
   * @throws DuplicatedPartnerIdException
   * @throws UnknownPartnerIdException
   */
  private void parseLine(String line) throws BadEntryException, UnknownProductIdException, DuplicatedPartnerIdException, UnknownPartnerIdException {
    String[] components = line.split("\\|");

    switch (components[0]) {
      case "PARTNER":
        parsePartner(components, line);
        break;
      case "BATCH_S":
        parseSimpleProduct(components, line);
        break;

      case "BATCH_M":
        parseAggregateProduct(components, line);
        break;
        
      default:
        throw new BadEntryException("Invalid type element: " + components[0]);
    }
  }

  /**
   * PARTNER|id|nome|endereço
   * @param components
   * @param line
   * @throws BadEntryException
   * @throws DuplicatedPartnerIdException
   */
  private void parsePartner(String[] components, String line) throws BadEntryException, DuplicatedPartnerIdException {
    if (components.length != 4)
      throw new BadEntryException("Invalid partner with wrong number of fields (4): " + line);
    
    String id = components[1];
    String name = components[2];
    String address = components[3];
    
    _warehouse.registerPartner(id, name, address);   
  }

  /**
   * BATCH_S|idProduto|idParceiro|preço|stock-actual
   * @param components
   * @param line
   * @throws BadEntryException
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   */
  private void parseSimpleProduct(String[] components, String line) throws BadEntryException, UnknownProductIdException, UnknownPartnerIdException {
    if (components.length != 5)
      throw new BadEntryException("Invalid number of fields (4) in simple batch description: " + line);
    
    String idProduct = components[1];
    String idPartner = components[2];
    double price = Double.parseDouble(components[3]);
    int stock = Integer.parseInt(components[4]);
    Product product = null;
    Partner partner = null;

    if (!_warehouse.productExists(idProduct)) {
      _warehouse.newSimpleProduct(idProduct); 
    }

    product = _warehouse.getProduct(idProduct);
    partner = _warehouse.getPartner(idPartner); 
    _warehouse.newBatch(product, partner, price, stock, true); 
  }
 
    
  /**
   * BATCH_M|idProduto|idParceiro|preço|stock-actual|agravamento|componente-1:quantidade-1#...#componente-n:quantidade-n
   * @param components
   * @param line
   * @throws BadEntryException
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   */
  private void parseAggregateProduct(String[] components, String line) throws BadEntryException, UnknownProductIdException, UnknownPartnerIdException {
    if (components.length != 7)
      throw new BadEntryException("Invalid number of fields (7) in aggregate batch description: " + line);
    
    String idProduct = components[1];
    String idPartner = components[2];
    Product prod = null; 
    double price = Double.parseDouble(components[3]);
    int stock = Integer.parseInt(components[4]);
    Partner partner = null;

    if (!_warehouse.productExists(idProduct)) {
      List<Product> products = new ArrayList<>();
      List<Integer> quantities = new ArrayList<>();
      
      for (String component : components[6].split("#")) {
        String[] recipeComponent = component.split(":");
        products.add(_warehouse.getProduct(recipeComponent[0]));
        quantities.add(Integer.parseInt(recipeComponent[1]));   
      }
      _warehouse.newAggregateProduct(idProduct, new Recipe(products, quantities), Double.parseDouble(components[5])); 
    }
    
    prod = _warehouse.getProduct(idProduct); 
    partner = _warehouse.getPartner(idPartner);    
    _warehouse.newBatch(prod, partner, price, stock, true);   
  }
}