package ggc.core;

/**
 * Periods
 */
public class Periods {

	/**
	 * 
	 * @return the the payment period
	 */
	public static Period dateDifferential(int n, int dueDate){
		if(dueDate-Date.now() >= n)
			return Period.P1;
		if(0 <= dueDate-Date.now()) //< n
			return Period.P2;
		if (Date.now()-dueDate > n)
			return Period.P4; 
		return Period.P3;
	}

    /**
     * 
     * @param dueDate
     * @param increaseByPercentage
     * @return the percentual adjustment for a fine
     */
    public static double getFine(int nDays, double increaseByPercentage){
        return -(nDays*increaseByPercentage); //-1 to adjust to the percentual adjustment
    }
}