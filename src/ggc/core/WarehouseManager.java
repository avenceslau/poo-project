package ggc.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.Collection;

import ggc.core.exception.BadEntryException;
import ggc.core.exception.DuplicatedPartnerIdException;
import ggc.core.exception.ImportFileException;
import ggc.core.exception.InvalidCoreDateException;
import ggc.core.exception.MissingFileAssociationException;
import ggc.core.exception.NotEnoughStockException;
import ggc.core.exception.UnavailableFileException;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;
import ggc.core.exception.UnknownTransactionIdException;
import ggc.core.partner.Partner;
import ggc.core.products.Batch;
import ggc.core.products.Product;
import ggc.core.products.Recipe;
import ggc.core.transactions.Transactions;


/** Facade for access. */
public class WarehouseManager {

  /** Name of file storing current warehouse. */
  private String _filename = "";

  /** The warehouse itself. */
  private Warehouse _warehouse = new Warehouse();

  /**
   * @@throws IOException
   * @@throws FileNotFoundException
   * @@throws MissingFileAssociationException
   */
  public void save() throws IOException, FileNotFoundException, MissingFileAssociationException {
    ObjectOutputStream out = null;
    FileOutputStream fileOut = null;
    try {
      fileOut = new FileOutputStream(_filename);
      out = new ObjectOutputStream(fileOut);
      out.writeObject(_warehouse);
      
   } catch (Exception e) {
      throw e;
  } finally {
      out.close();
      fileOut.close();
  }
  }

  /**
   * saves the warehouse to a file
   * @@param filename
   * @@throws MissingFileAssociationException
   * @@throws IOException
   * @@throws FileNotFoundException
   */
  public void saveAs(String filename) throws MissingFileAssociationException, FileNotFoundException, IOException {
    _filename = filename;
    save();
  }

  /**
   * loads a warehouse from a file
   * @param filename
   * @throws UnavailableFileException
   * @throws ClassNotFoundException
   * @throws IOException
   */
  public void load(String filename) throws UnavailableFileException, ClassNotFoundException, IOException {
  FileInputStream fileIn = null; 
  ObjectInputStream in = null;
  try {
    fileIn = new FileInputStream(filename);
    in = new ObjectInputStream(fileIn);
    _warehouse = (Warehouse) in.readObject();
    _filename = filename;
  } catch (Exception e) {
    throw e;
  } finally {
      in.close();
      fileIn.close();
   }
  }

  /**
   * imports a file
   * @param textfile
   * @throws ImportFileException
   */
  public void importFile(String textfile) throws ImportFileException {
    try {
      _warehouse.importFile(textfile);
    } catch (IOException | BadEntryException | UnknownPartnerIdException | UnknownProductIdException | DuplicatedPartnerIdException e) {
      throw new ImportFileException(textfile, e);
    }
  }

  /**
   * 
   * @return A list of stringified products 
   */
  public Collection<String> showAllProducts() {
    return _warehouse.showAllProducts();
  }

  /**
   * 
   * @return the available balance in the warehouse
   */
  public double getAvailableBalance(){
    return _warehouse.getAvailableBalance();
  }

  /**
   * 
   * @return the accounting balance from the warehouse
   */
  public double getAccountingBalance(){
    return _warehouse.getAccountingBalance();
  }

  /**
   * 
   * @return all batches ordered
   */
  public Collection<Batch> showAllBatches(){
    return _warehouse.getAllBatchesOrdered();
  }

  /**
   * 
   * @param partnerId id of the partner
   * @return a stringified partner
   * @throws UnknownPartnerID
   */
  public String showPartner(String partnerId) throws UnknownPartnerIdException {
    return _warehouse.showPartner(partnerId);  
  }

  /**
   * 
   * @return a collection of partners 
   */
  public Collection<Partner> showAllPartners(){
    return _warehouse.getPartners();
  }

  /**
   * 
   * @param partnerId
   * @return a collection of strings where which string is a notification
   * @throws UnknownPartnerIdException
   */
  public Collection<String> getNotificationsFrom(String partnerId) throws UnknownPartnerIdException {
    return _warehouse.getNotificationsFrom(partnerId); 
  }

  /**
   * registers a new partner
   * @param partnerId
   * @param partnerName
   * @param partnerAddress
   * @throws DuplicatedPartnerID
   */
  public void registerPartner(String partnerId, String partnerName, String partnerAddress) throws DuplicatedPartnerIdException {
    _warehouse.registerPartner(partnerId, partnerName, partnerAddress);
  }

  /**
   * 
   * @return true if there is a save file already associated 
   */
  public boolean hasAssociatedFile(){
    return (!_filename.equals(""));
  }
  
  /**
   * 
   * @param days must be higher than 0
   * @throws InvalidDate
   */
  public void advanceDate(int days) throws InvalidCoreDateException {
    _warehouse.advanceDate(days); 
  }

  /**
   * 
   * @return the current date
   */
  public int getDate(){
    return _warehouse.getDate();
  }
  
  /**
   * 
   * @param partnerId
   * @return batches created from a specific partner
   * @throws UnknownPartnerIdException
   */
  public Collection<Batch> showBatchesByPartner(String partnerId) throws UnknownPartnerIdException{
    return _warehouse.getBatchesFromPartner(partnerId);
  }

  /**
   * 
   * @param productId
   * @return batches created from a specific product 
   * @throws UnknownProductIdException
   */
  public Collection<Batch> showBatchesByProduct(String productId) throws UnknownProductIdException{ 
    return _warehouse.showBatchesForProduct(productId);
  }

  /**
   * toggles notifications on and off from a product on a specific partner
   * @param partnerId
   * @param productId
   * @throws UnknownPartnerIdException
   * @throws UnknownProductIdException
   */
  public void toggleNotificationFor(String partnerId, String productId) throws UnknownPartnerIdException, UnknownProductIdException{
    _warehouse.toggleNotification(partnerId, productId);
  }

  /**
   * 
   * @param id
   * @return a product
   * @throws UnknownProductIdException
   */
  public Product getProduct(String id) throws UnknownProductIdException{
    return _warehouse.getProduct(id);
  }

  /**
   * registers a newSimple product
   * @param productId
   */
  public void newSimpleProduct(String productId) {
    _warehouse.newSimpleProduct(productId); 
  }

  /**
   * registers a new aggregate product
   * @param productId
   * @param recipe
   * @param aggravation
   */
  public void newAggregateProduct(String productId, Recipe recipe, double aggravation){
    _warehouse.newAggregateProduct(productId, recipe, aggravation);
  }

  /**
   * registers a new acquisition
   * @param productQuantity
   * @param partnerId
   * @param productId
   * @param productPrice
   * @throws UnknownPartnerIdException
   */
  public void registerAcquisition(int productQuantity, String partnerId, String productId, double productPrice) throws UnknownPartnerIdException {
    _warehouse.registerAcquisition(productQuantity, partnerId, productId, productPrice);
  }

  /**
   * 
   * @param partnerId
   * @return acquisitions made by a partner
   * @throws UnknownPartnerIdException
   */
  public Collection<Transactions> showAcquisitionsByPartner(String partnerId) throws UnknownPartnerIdException{
    return _warehouse.showAcquisitionsByPartner(partnerId);
  }

  /**
   * 
   * @param partnerId
   * @return sales made by a partner
   * @throws UnknownPartnerIdException
   */
  public Collection<Transactions> showSalesByPartner(String partnerId) throws UnknownPartnerIdException{
    return _warehouse.showSalesByPartner(partnerId);
  }

  /**
   * 
   * @param transactionId
   * @return a transaction
   * @throws UnknownTransactionIdException
   */
  public Transactions getTransaction(int transactionId) throws UnknownTransactionIdException{
    return _warehouse.getTransaction(transactionId);
  }

  /**
   * 
   * @param priceLimit
   * @return batches under a given price
   */
  public Collection<Batch> showBatchesUnderPrice(double priceLimit) {
    return _warehouse.showBatchesUnderPrice(priceLimit);
  }

  /**
   * registers a new sale by credit
   * @param partner
   * @param product
   * @param requestedAmount
   * @param requestPaymentDeadline
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   * @throws InvalidCoreDateException
   * @throws NotEnoughStockException
   */
  public void registerSaleByCredit(String partner, String product, int requestedAmount, int requestPaymentDeadline) throws UnknownProductIdException, UnknownPartnerIdException, InvalidCoreDateException, NotEnoughStockException{
    _warehouse.registerSaleByCredit(partner, product, requestedAmount, requestPaymentDeadline);
  }

  /**
   * registers a new breakdown sale
   * @param productQuantity
   * @param partner
   * @param product
   * @throws NotEnoughStockException
   * @throws UnknownProductIdException
   * @throws UnknownPartnerIdException
   */
  public void registerBreakdownSale(int productQuantity, String partner, String product) throws NotEnoughStockException, UnknownProductIdException, UnknownPartnerIdException{
    _warehouse.registerBreakdownSale(productQuantity, partner, product);
  }

  /**
   * receives payment for a transaction if it is a sale
   * if not nothing happens
   * @param transactionId
   * @throws UnknownTransactionIdException
   */
  public void receivePayment(int transactionId) throws UnknownTransactionIdException {
    _warehouse.receivePayment(transactionId);
  }

  /**
   * 
   * @param partnerId
   * @return payments made by a specific partner
   * @throws UnknownPartnerIdException
   */
  public Collection<Transactions> showPaymentsByPartner(String partnerId) throws UnknownPartnerIdException{
    return _warehouse.showPaymentsByPartner(partnerId);
  }
}
