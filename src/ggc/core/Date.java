package ggc.core;


public class Date {
	private static int _days = 0;
	private Date() {
		super();
	}	

	/**
	* A getter method for current date
	*
	* @return the current date
	*/
	public static int now() {
		return _days;
	}

	/**
	* A method to advance a given 
	* number of days
	*/
	public static void advanceDays(int days) {
		_days += days;
	}

	/**
	 * should only be used when serializing warehouse
	 * @param _days
	 */
	protected static void setDays(int days) {
		_days = days;
	}
}