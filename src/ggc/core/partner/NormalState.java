package ggc.core.partner;

import java.io.Serializable;

import ggc.core.Date;
import ggc.core.Period;
import ggc.core.Periods;

/**
 * NormalState
 */
public class NormalState implements Status, Serializable {
    private static final long serialVersionUID = 266403984005996810l;

    /**
     * 
     * @return string representation of the tier
     */
    @Override
    public String getTier() {
        return "NORMAL";
    }

    /**
     * Constructor
     */
    protected NormalState() {
        super();
    }

    /**
     * Apply fine and discount based on dueDate
     * @param dueDate
     * @param n
     * @return the percentual change based on the the due date and N
     */
    @Override
    public double getCostModifier(int dueDate, int n) {
        Period period = Periods.dateDifferential(n, dueDate);
        if (period.equals(Period.P1)) 
            return 0.10;   
        if(period.equals(Period.P2))
            return 0;
        if (period.equals(Period.P3)) {
            return Periods.getFine(Date.now()-dueDate, 0.05);
        }
        return Periods.getFine(Date.now()-dueDate, 0.10);
    }

    /**
     * Demotes partner to a lower state
     * In this case there is no lower state so it stays the same
     * @param partner
     * @param dueDate
     */
    @Override
    public void loseState(Partner partner, int dueDate) {
        partner.setPoints(0);
    }

    /**
     * Promotes partner to a higher state
     * @param partner
     * @param currentState
     */
    @Override
    public void advanceState(Partner partner, Status currentState) {
        if (partner.getPoints() > 2000) {
            partner.setStatus(new SelectionState());
            partner.getStatus().advanceState(partner, partner.getStatus());
        }
    }
}