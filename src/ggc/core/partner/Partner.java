package ggc.core.partner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ggc.core.notifications.Notifiable;
import ggc.core.notifications.NotificationToScreen;
import ggc.core.notifications.Notifications;
import ggc.core.notifications.SendNotificationMode;


/**
 * Partner
 */
public class Partner implements Serializable, Notifiable {

    /** Serial number for serialization. */
    private static final long serialVersionUID = 21201210l;
    private String _id = ""; 
    private String _name = "";
    private String _address = ""; 
    private Status _status = new NormalState();         
    private double _points = 0;
    private double _purchases = 0; //Value of purchases made by warehouse to partner
    private double _totalSales = 0; //Value of sales made by warehouse to partner
    private double _paidSales = 0; //Value of paid sales made by warehouse to partner 
    private List<Notifications> _notifications = new ArrayList<Notifications>(); //NULL and false means that the partner receives notifications for the product
    private SendNotificationMode _mode = new NotificationToScreen();

    /**
     * 
     * @param num
     * @return rounds num to int
     */
    private int round(double num){
        return (int) Math.round(num);
    }

    /**
     * 
     * @return the send mode
     */
    @Override
    public SendNotificationMode getSendMode() {
        return _mode;
    }

    /**
     * changes the send mode
     * @param newMode
     */
    @Override
    public void setSendMode(SendNotificationMode newMode) {
        _mode = newMode;
    }

    /**
     * Constructor
     * 
     * @param id
     * @param name
     * @param address
     */
    public Partner(String id, String name, String address) {
        super();
        _id = id;
        _name = name;
        _address = address;        
    }

    /**
     * 
     * @return partner's address
     */
    public String getAddress() {
        return _address;
    }

    /**
     * 
     * @return partner's id
     */
    public String getId(){
        return _id;
    }

    /**
     * 
     * @return partner's name 
     */
    public String getName() {
        return _name;
    }
    
    /**
     * 
     * @return string representation of partner's status 
     */
    protected String getStatusName() {
        return _status.getTier();
    }    

    /**
     * 
     * @return partner's status
     */
    protected Status getStatus() {
        return _status;
    }
    
    /**
     * 
     * @return points of the partner
     */
    public double getPoints() {
        return _points;
    }

    /**
     * Adds points after a purchase and increases status
     * @param points amount of points to be added
     */
    public void addPoints(double points) {
        if(points == 0)
            return;
        _points+=points;
        _status.advanceState(this, getStatus()); 
    }

    /**
     * Subtracts points based on the due date and updates Partner Status
     * @param dueDate
     */
    public void losePoints(int dueDate){
        getStatus().loseState(this, dueDate);
    }
    
    /**
     * @param status
     */
    protected void setStatus(Status status) {
        _status = status;
    }

    /**
     * 
     * @param points
     */
    protected void setPoints(double points) {
        _points = points;
    }
    /**
     * 
     * @return value of total purchases
     */
    private double getPurchases() {
        return _purchases;
    }

    /**
     * 
     * @return value of total sales
     */
    private double getTotalSales() {
        return _totalSales;
    }

    /**
     * Apply fine and discount based on dueDate
     * @param dueDate
     * @param n
     * @return the percentual change based on the the due date and N
     */
    public double getCostModifier(int dueDate, int n){
		return 1-_status.getCostModifier(dueDate, n);
    }

    /**
     * 
     * @return value of paid sales
     */
    private double getPaidSales() {
        return _paidSales;
    }

    /**
     * increases the value of purchases
     * @param byAmount
     */
    public void updatePurchases(double byAmount){
        _purchases += byAmount;
    }

    /**
     * increases the value of total sales
     * @param byAmount
     */
    public void updateTotalSales(double byAmount){
        _totalSales += byAmount;
    }
    
    /**
     * increases the value of payed sales
     * @param byAmount
     */
    public void updatePayedSales(double byAmount){
        _paidSales += byAmount;
    }

    @Override
    public String toString() {
        return getId()+"|"+getName()+"|"+getAddress()+"|"+getStatusName()+"|"+ round(getPoints())+"|"+ round(getPurchases())+"|"+ round(getTotalSales())+"|"+ round(getPaidSales());
    }


    /**
     * creates a list of strings with all the notifications received
     * than clears all the notifications
     * @return list of notifications in string form
     */
    public List<String> getNotifications(){
        List<String> l = new ArrayList<String>();
        for (Notifications notification : _notifications) {
            l.add(notification.toString());
        }

        _notifications.clear();
       return l; 
    }

    /**
     * add new Notification
     * @param notification 
     */
    private void addNotification(Notifications notification){
        _notifications.add(notification);
    }

    /**
     * @param notification
     */
    @Override
    public void addNotifications(Notifications notification) {
        addNotification(notification);
    }
    
}