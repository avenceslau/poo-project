package ggc.core.partner;

import java.io.Serializable;

import ggc.core.Date;
import ggc.core.Period;
import ggc.core.Periods;

/**
 * SelectionState
 */
public class SelectionState implements Status, Serializable {
    private static final long serialVersionUID = 26649084005996810l;

    /**
     * 
     * @return string representation of the tier
     */
    @Override
    public String getTier() {
        return "SELECTION";
    }

    /**
     * Constructor
     */
    protected SelectionState() {
        super();
    }
    
    /**
     * Apply fine and discount based on dueDate
     * @param dueDate
     * @param n
     * @return the percentual change based on the the due date and N
     */
    @Override
    public double getCostModifier(int dueDate, int n) {
        Period period = Periods.dateDifferential(n, dueDate);
        if (period.equals(Period.P1)) 
            return 0.10;   
        if(period.equals(Period.P2)){
            if (dueDate-Date.now() >= 2) {
                return 0.05;
            }
            return 0;
        }
        if (period.equals(Period.P3)) {
            if (dueDate-Date.now() >= -1) {
                return 0;
            }
            return Periods.getFine((Date.now()-dueDate), 0.02);
        }
        return Periods.getFine(Date.now()-dueDate, 0.05);
    }

    /**
     * Demotes partner to a lower state
     * @param partner
     * @param dueDate
     */
    @Override
    public void loseState(Partner partner, int dueDate) {
        if((Date.now()-dueDate) > 2){
            partner.setPoints(partner.getPoints()*0.10);
            partner.setStatus(new NormalState());
            return;
        }
    }
    
    /**
     * Promotes partner to a higher state
     * @param partner
     * @param currentState
     */
    @Override
    public void advanceState(Partner partner, Status currentState) {
        if (partner.getPoints() > 25000) {
            partner.setStatus(new EliteState());
            partner.getStatus().advanceState(partner, partner.getStatus());
        }
        
    }
}