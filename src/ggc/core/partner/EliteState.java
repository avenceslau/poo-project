package ggc.core.partner;

import java.io.Serializable;

import ggc.core.Date;
import ggc.core.Period;
import ggc.core.Periods;

/**
 * EliteState
 */
public class EliteState implements Status, Serializable {
    
    private static final long serialVersionUID = 399403984005996810l;

    /**
     * Constructor
     */
    protected EliteState() {
        super();
    }
    
    /**
     * 
     * @return string representation of the tier
     */
    @Override
    public String getTier() {
        return "ELITE";
    }

    /**
     * Apply fine and discount based on dueDate
     * @param dueDate
     * @param n
     * @return the percentual change based on the the due date and N
     */
    @Override
    public double getCostModifier(int dueDate, int n) {
        Period period = Periods.dateDifferential(n, dueDate);
        if (period.equals(Period.P1) || period.equals(Period.P2)) 
            return 0.10;   
        if (period.equals(Period.P3)) {
            return 0.05;
        }
        return 0;
    }
    
    /**
     * Demotes partner to a lower state
     * @param partner
     * @param dueDate
     */
    @Override
    public void loseState(Partner partner, int dueDate) {
        if((Date.now()-dueDate) > 15){
            partner.setPoints(0.25*partner.getPoints());
            partner.setStatus(new SelectionState());
            return;
        }
    }

    /**
     * Promotes partner to a higher state
     * In this case there is no higher state
     * @param partner
     * @param currentState
     */
    @Override
    public void advanceState(Partner partner, Status currentState) {
        return;
    }

}