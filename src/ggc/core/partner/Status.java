package ggc.core.partner;

/**
 * Status
 */
public interface Status {
    /**
     * 
     * @return string representation of the tier
     */
    String getTier(); 
    
    /**
     * Apply fine and discount based on dueDate
     * @param dueDate
     * @param n
     * @return the percentual change based on the the due date and N
     */
    double getCostModifier(int dueDate, int n);
   
    /**
     * Demotes partner to a lower state
     * @param partner
     * @param dueDate
     */
    void loseState(Partner partner, int dueDate);
    
    /**
     * Promotes partner to a higher state
     * @param partner
     * @param currentState
     */
    void advanceState(Partner partner, Status currentState);
}

