package ggc.app.transactions;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnavailableProductException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.app.exception.UnknownProductKeyException;
import ggc.core.WarehouseManager;
import ggc.core.exception.NotEnoughStockException;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;

/**
 * Register order.
 */
public class DoRegisterBreakdownTransaction extends Command<WarehouseManager> {

  public DoRegisterBreakdownTransaction(WarehouseManager receiver) {
    super(Label.REGISTER_BREAKDOWN_TRANSACTION, receiver);
  } 

  @Override
  public final void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    String productId = Form.requestString(Message.requestProductKey()); 
    int productQuantity = Form.requestInteger(Message.requestAmount());
    
    try {
      _receiver.registerBreakdownSale(productQuantity, partnerId, productId);
    } catch (NotEnoughStockException e) {
      throw new UnavailableProductException(e.getProductId(), e.getNeededStock(), e.getAvailableStock());
    } catch (UnknownProductIdException e) {
      throw new UnknownProductKeyException(productId);    
    } catch (UnknownPartnerIdException e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
  }

}
