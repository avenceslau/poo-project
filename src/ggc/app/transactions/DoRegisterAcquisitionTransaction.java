package ggc.app.transactions;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;

import java.util.ArrayList;
import java.util.List;

import ggc.app.exception.UnknownPartnerKeyException;
import ggc.app.exception.UnknownProductKeyException;
import ggc.core.WarehouseManager;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.products.Product;
import ggc.core.products.Recipe;


/**
 * Register order.
 */
public class DoRegisterAcquisitionTransaction extends Command<WarehouseManager> {

  public DoRegisterAcquisitionTransaction(WarehouseManager receiver) {
    super(Label.REGISTER_ACQUISITION_TRANSACTION, receiver);
  }

  private Recipe getRecipe(int numberOfComponents) throws UnknownProductKeyException{
    List<Product> products = new ArrayList<Product>();
    List<Integer> quantities = new ArrayList<Integer>();
    String requestProductId = null;
    int requestQuantities = 0;
    
    for (int i = 0; i < numberOfComponents ; i++) {
      requestProductId = Form.requestString(Message.requestProductKey());
      requestQuantities = Form.requestInteger(Message.requestAmount());
      try {
        products.add(_receiver.getProduct(requestProductId)); 
      } catch (Exception e) {
        throw new UnknownProductKeyException(requestProductId);
      }
      quantities.add(requestQuantities);      
    }
    return new Recipe(products, quantities);
  }

  @Override
  public final void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    String productId = Form.requestString(Message.requestProductKey());
    double productPrice = Form.requestReal(Message.requestPrice());
    int productQuantity = Form.requestInteger(Message.requestAmount());
     
    try {
      _receiver.getProduct(productId);
    } catch (Exception e) {
      if(Form.confirm(Message.requestAddRecipe())) {
        int requestNumberOfComponents = Form.requestInteger(Message.requestNumberOfComponents());
        double requestAlpha = Form.requestReal(Message.requestAlpha());
        _receiver.newAggregateProduct(productId, getRecipe(requestNumberOfComponents), requestAlpha);
      } else {
        _receiver.newSimpleProduct(productId);
      }
    } 

    try { 
      _receiver.registerAcquisition(productQuantity, partnerId, productId, productPrice);
    } catch (UnknownPartnerIdException e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
  }

}

