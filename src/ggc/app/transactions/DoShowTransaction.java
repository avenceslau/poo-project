package ggc.app.transactions;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownTransactionKeyException;
import ggc.core.WarehouseManager;

/**
 * Show specific transaction.
 */
public class DoShowTransaction extends Command<WarehouseManager> {

  public DoShowTransaction(WarehouseManager receiver) {
    super(Label.SHOW_TRANSACTION, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    int transactionId = Form.requestInteger(Message.requestTransactionKey());
    try {
      _display.add(_receiver.getTransaction(transactionId));
      _display.display();
    } catch (Exception e) {
      throw new UnknownTransactionKeyException(transactionId);
    }
  }

}
