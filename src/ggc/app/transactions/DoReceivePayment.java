package ggc.app.transactions;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownTransactionKeyException;
import ggc.core.WarehouseManager;

/**
 * Receive payment for sale transaction.
 */
public class DoReceivePayment extends Command<WarehouseManager> {

  public DoReceivePayment(WarehouseManager receiver) {
    super(Label.RECEIVE_PAYMENT, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    int transactionId = Form.requestInteger(Message.requestTransactionKey());

    try {
      _receiver.receivePayment(transactionId);
    } catch (Exception e) {
      throw new UnknownTransactionKeyException(transactionId);
    }

  }

}
