package ggc.app.transactions;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.InvalidDateException;
import ggc.app.exception.UnavailableProductException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.app.exception.UnknownProductKeyException;
import ggc.core.WarehouseManager;
import ggc.core.exception.NotEnoughStockException;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;
import ggc.core.exception.InvalidCoreDateException;


/**
 * 
 */
public class DoRegisterSaleTransaction extends Command<WarehouseManager> {

  public DoRegisterSaleTransaction(WarehouseManager receiver) {
    super(Label.REGISTER_SALE_TRANSACTION, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    int requestPaymentDeadline = Form.requestInteger(Message.requestPaymentDeadline());
    String productId =  Form.requestString(Message.requestProductKey());
    int requestedAmount =  Form.requestInteger(Message.requestAmount());
  
    
    try {
      _receiver.registerSaleByCredit(partnerId, productId, requestedAmount, requestPaymentDeadline);
    } catch (NotEnoughStockException e) {
      throw new UnavailableProductException(e.getProductId(), e.getNeededStock(), e.getAvailableStock());
    } catch (UnknownProductIdException e) {
      throw new UnknownProductKeyException(productId);
    } catch (InvalidCoreDateException e) {
      throw new InvalidDateException(requestPaymentDeadline);
    } catch (UnknownPartnerIdException e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
    

  }



}
