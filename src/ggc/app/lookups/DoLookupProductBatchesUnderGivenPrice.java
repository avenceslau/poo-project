package ggc.app.lookups;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.core.WarehouseManager;

/**
 * Lookup products cheaper than a given price.
 */
public class DoLookupProductBatchesUnderGivenPrice extends Command<WarehouseManager> {

  public DoLookupProductBatchesUnderGivenPrice(WarehouseManager receiver) {
    super(Label.PRODUCTS_UNDER_PRICE, receiver);
  }

  @Override
  public void execute() throws CommandException {
    double priceLimit = Form.requestReal(Message.requestPriceLimit());
    _display.addAll(_receiver.showBatchesUnderPrice(priceLimit));
    _display.display();
  }

}
