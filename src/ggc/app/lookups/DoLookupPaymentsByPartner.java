package ggc.app.lookups;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.core.WarehouseManager;

/**
 * Lookup payments by given partner.
 */
public class DoLookupPaymentsByPartner extends Command<WarehouseManager> {

  public DoLookupPaymentsByPartner(WarehouseManager receiver) {
    super(Label.PAID_BY_PARTNER, receiver);
  }

  @Override
  public void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    
    try {
      _display.addAll(_receiver.showPaymentsByPartner(partnerId));
    } catch (Exception e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
    _display.display();
  }
}
