package ggc.app.main;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;

import ggc.core.WarehouseManager;

/**
 * Save current state to file under current name (if unnamed, query for name).
 */
class DoSaveFile extends Command<WarehouseManager> {

  /** @param receiver */
  DoSaveFile(WarehouseManager receiver) {
    super(Label.SAVE, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    String filename = "";
    try {
      if(!_receiver.hasAssociatedFile()) {
        filename = Form.requestString(Message.newSaveAs());
        _receiver.saveAs(filename);
      }
      _receiver.save(); 
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
