package ggc.app.main;

import ggc.core.WarehouseManager;
import pt.tecnico.uilib.menus.DoOpenMenu;

/** Main menu. */
public class Menu extends pt.tecnico.uilib.menus.Menu {

  /** @param receiver command executor */
  public Menu(WarehouseManager receiver) {
    super(Label.TITLE, //Menu Title
        new DoOpenFile(receiver), // Open previous state via file
        new DoSaveFile(receiver), // Save state to file
        new DoDisplayDate(receiver), // Shows current date
        new DoAdvanceDate(receiver), // Advances date by x days x is user input
        new DoOpenMenu(Label.OPEN_MENU_PRODUCTS, new ggc.app.products.Menu(receiver)), // Open products menu
        new DoOpenMenu(Label.OPEN_MENU_PARTNERS, new ggc.app.partners.Menu(receiver)), // Open partners menu
        new DoOpenMenu(Label.OPEN_MENU_TRANSACTIONS, new ggc.app.transactions.Menu(receiver)), // Open transactions menu
        new DoOpenMenu(Label.OPEN_MENU_LOOKUPS, new ggc.app.lookups.Menu(receiver)), // Open lookups menu
        new DoShowGlobalBalance(receiver) // Show global balance
    );
  }

}
