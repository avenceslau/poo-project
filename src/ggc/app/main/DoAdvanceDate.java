package ggc.app.main;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.InvalidDateException;
import ggc.core.WarehouseManager;

/**
 * Advance current date.
 */
class DoAdvanceDate extends Command<WarehouseManager> {
  DoAdvanceDate(WarehouseManager receiver) {
    super(Label.ADVANCE_DATE, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    int days = Form.requestInteger(Message.requestDaysToAdvance());
      try {
        _receiver.advanceDate(days);  
      } catch (Exception e) {
        throw new InvalidDateException(days);
      }
    
  }

}
