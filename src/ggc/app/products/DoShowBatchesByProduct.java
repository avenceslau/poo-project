package ggc.app.products;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownProductKeyException;
import ggc.core.WarehouseManager;

/**
 * Show all products.
 */
class DoShowBatchesByProduct extends Command<WarehouseManager> {

  DoShowBatchesByProduct(WarehouseManager receiver) {
    super(Label.SHOW_BATCHES_BY_PRODUCT, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    String productId = Form.requestString(Message.requestProductKey());
    try {
      _display.addAll(_receiver.showBatchesByProduct(productId));
      _display.display();
    } catch (Exception e) {
      throw new UnknownProductKeyException(productId);
    }
  }

}
