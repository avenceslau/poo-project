package ggc.app.products;

import ggc.core.WarehouseManager;

/** Products menu. */
public class Menu extends pt.tecnico.uilib.menus.Menu {

  /** @param receiver command executor */
  public Menu(WarehouseManager receiver) {
    super(Label.TITLE, // Show Title
        new DoShowAllProducts(receiver), // Show all products
        new DoShowAvailableBatches(receiver), // Show all Batches
        new DoShowBatchesByPartner(receiver), // Show Batches for each partner
        new DoShowBatchesByProduct(receiver) // Show Batches for each product
    );
  }

}
