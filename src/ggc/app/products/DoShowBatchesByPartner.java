package ggc.app.products;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.core.WarehouseManager;

/**
 * Show batches supplied by partner.
 */
class DoShowBatchesByPartner extends Command<WarehouseManager> {

  DoShowBatchesByPartner(WarehouseManager receiver) {
    super(Label.SHOW_BATCHES_SUPPLIED_BY_PARTNER, receiver);
  }

  @Override
  public final void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    try {
      _display.addAll(_receiver.showBatchesByPartner(partnerId));
      _display.display();
    } catch (Exception e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
  }
}
