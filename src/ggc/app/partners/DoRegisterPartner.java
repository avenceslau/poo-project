package ggc.app.partners;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.DuplicatePartnerKeyException;
import ggc.core.WarehouseManager;
import ggc.core.exception.DuplicatedPartnerIdException;

/**
 * Register new partner.
 */
class DoRegisterPartner extends Command<WarehouseManager> {

  DoRegisterPartner(WarehouseManager receiver) {
    super(Label.REGISTER_PARTNER, receiver);
  }

  @Override
  public void execute() throws CommandException {
    String partnerId = "";
    try {
      partnerId = Form.requestString(Message.requestPartnerKey());
      String partnerName = Form.requestString(Message.requestPartnerName());
      String partnerAddress = Form.requestString(Message.requestPartnerAddress());
      _receiver.registerPartner(partnerId, partnerName, partnerAddress);
    } catch (DuplicatedPartnerIdException e) {
      throw new DuplicatePartnerKeyException(partnerId);
    }
  }

}
