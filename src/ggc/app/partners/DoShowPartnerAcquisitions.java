package ggc.app.partners;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.core.WarehouseManager;

/**
 * Show all transactions for a specific partner.
 */
class DoShowPartnerAcquisitions extends Command<WarehouseManager> {

  DoShowPartnerAcquisitions(WarehouseManager receiver) {
    super(Label.SHOW_PARTNER_ACQUISITIONS, receiver);
  }

  @Override
  public void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    try {
      _display.addAll(_receiver.showAcquisitionsByPartner(partnerId));
      _display.display();
    } catch (Exception e) {
      throw new UnknownPartnerKeyException(partnerId);
    }
  }

}
