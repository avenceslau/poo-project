package ggc.app.partners;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.app.exception.UnknownProductKeyException;
import ggc.core.WarehouseManager;
import ggc.core.exception.UnknownPartnerIdException;
import ggc.core.exception.UnknownProductIdException;

/**
 * Toggle product-related notifications.
 */
class DoToggleProductNotifications extends Command<WarehouseManager> {

  DoToggleProductNotifications(WarehouseManager receiver) {
    super(Label.TOGGLE_PRODUCT_NOTIFICATIONS, receiver);
  }

  @Override
  public void execute() throws CommandException {
    String partnerId = Form.requestString(Message.requestPartnerKey());
    String productId = Form.requestString(Message.requestProductKey());
    try {
      _receiver.toggleNotificationFor(partnerId, productId);
    } catch (UnknownPartnerIdException e) {
      throw new UnknownPartnerKeyException(partnerId);
    } catch (UnknownProductIdException e) {
      throw new UnknownProductKeyException(productId);
    }
  }

}
