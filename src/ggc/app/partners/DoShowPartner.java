package ggc.app.partners;

import pt.tecnico.uilib.forms.Form;
import pt.tecnico.uilib.menus.Command;
import pt.tecnico.uilib.menus.CommandException;
import ggc.app.exception.UnknownPartnerKeyException;
import ggc.core.WarehouseManager;

/**
 * Show partner.
 */
class DoShowPartner extends Command<WarehouseManager> {

  DoShowPartner(WarehouseManager receiver) {
    super(Label.SHOW_PARTNER, receiver);
  }

  @Override
  public void execute() throws CommandException {
      String partnerId = Form.requestString(Message.requestPartnerKey());
      try {
        _display.add(_receiver.showPartner(partnerId)); 
        _display.addAll(_receiver.getNotificationsFrom(partnerId));
        _display.display();
      } catch (Exception e) {
        throw new UnknownPartnerKeyException(partnerId);
      }
  }

}
