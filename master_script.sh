#!/bin/bash

javac -d bin -cp libs/po-uilib-v11.jar:. `find src/ggc -name "*.java"`
rm tests/*diff
./runtests.sh
rm *.dat
rm *.ggc
cd src
jar cvf GR_88.jar ggc >/dev/null 
mv GR_88.jar ..  
cd ..
